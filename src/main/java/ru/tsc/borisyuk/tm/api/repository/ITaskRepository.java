package ru.tsc.borisyuk.tm.api.repository;

import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    void clear();

    List<Task> findAll();

}
