package ru.tsc.borisyuk.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
