package ru.tsc.borisyuk.tm.api.repository;

import ru.tsc.borisyuk.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
