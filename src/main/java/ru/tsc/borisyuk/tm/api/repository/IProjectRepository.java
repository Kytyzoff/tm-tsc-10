package ru.tsc.borisyuk.tm.api.repository;

import ru.tsc.borisyuk.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

}
