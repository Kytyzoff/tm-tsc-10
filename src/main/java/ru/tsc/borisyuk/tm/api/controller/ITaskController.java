package ru.tsc.borisyuk.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
