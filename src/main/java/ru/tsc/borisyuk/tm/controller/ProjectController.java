package ru.tsc.borisyuk.tm.controller;

import ru.tsc.borisyuk.tm.api.controller.IProjectController;
import ru.tsc.borisyuk.tm.api.service.IProjectService;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW PROJECTS]");
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

}
